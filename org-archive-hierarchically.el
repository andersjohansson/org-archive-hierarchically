;;; org-archive-hierarchically.el --- Archive org-mode subtrees hierarchically  -*- lexical-binding: t -*-

;; Copyright (C) 2015-2019 Florian Adamsky, Anders Johansson

;; Author: Florian Adamsky
;; Maintainer: Anders Johansson <mejlaandersj@gmail.com>
;; Version: 0.1
;; Created 2015
;; Modified: 2021-04-06
;; Package-Requires: ((org "9.2") (emacs "24.1"))
;; Keywords: outlines, wp
;; URL: http://www.gitlab.com/andersjohansson/org-archive-hierarchically

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; Defines a command `org-archive-hierarchically' that archives a
;; subtree under the same hierarchy as it is currently filed. That is,
;; it searches in the archive file and creates the tree above the
;; entry to be archived if it is not found.
;; Based on:
;; https://stackoverflow.com/a/35475878
;; and:
;; https://gist.github.com/kepi/2f4acc3cc93403c75fbba5684c5d852d
;;
;; TODO: Remove statistics cookies from the archive? Makes matching of
;; headlines file so that duplicate are created for subtasks when the
;; parent headline had.

;;; Code:

(require 'org-archive)

;;;###autoload
(defun org-archive-hierarchically ()
  "Archive a subtree hierarchically."
  (interactive)
  (let* ((tr-org-todo-keywords-1 org-todo-keywords-1)
         (tr-org-todo-kwd-alist org-todo-kwd-alist)
         (tr-org-done-keywords org-done-keywords)
         (tr-org-todo-regexp org-todo-regexp)
         (tr-org-todo-line-regexp org-todo-line-regexp)
         (tr-org-odd-levels-only org-odd-levels-only)
         (this-buffer (current-buffer))
         (time (format-time-string
                (substring (cdr org-time-stamp-formats) 1 -1)))
         (file (abbreviate-file-name
                (or (buffer-file-name (buffer-base-buffer))
                    (error "No file associated to buffer"))))
         (location org-archive-location)
         (afile (or (car (org-archive--compute-location
                          (or (org-entry-get nil "ARCHIVE" 'inherit)
                              location)))
                    (error "Invalid `org-archive-location'")))
         (infile-p (equal file (abbreviate-file-name (or afile ""))))
         (buffer (cond ((not (org-string-nw-p afile)) this-buffer)
                       ((find-buffer-visiting afile))
                       ((find-file-noselect afile))
                       (t (error "Cannot access file \"%s\"" afile))))
         (org-tree (org-archive-hierarchically--org-struct-subtree)))
    (save-excursion
      (org-back-to-heading t)
      ;; Get context information that will be lost by moving the
      ;; tree.  See `org-archive-save-context-info'.
      (let* ((all-tags (org-get-tags))
             (local-tags (org-get-tags nil t))
             (inherited-tags (org-delete-all local-tags all-tags))
             (context
              `((category . ,(org-get-category nil 'force-refresh))
                (file . ,file)
                (itags . ,(mapconcat #'identity inherited-tags " "))
                (ltags . ,(mapconcat #'identity local-tags " "))
                (olpath . ,(mapconcat #'identity
                                        (org-get-outline-path)
                                        "/"))
                (time . ,time)
                (todo . ,(org-entry-get (point) "TODO")))))
        ;; We first only copy, in case something goes wrong
        ;; we need to protect `this-command', to avoid kill-region sets it,
        ;; which would lead to duplication of subtrees
        (let (this-command) (org-copy-subtree 1 nil t))
        (with-current-buffer buffer
          ;; Enforce org-mode for the archive buffer
          (if (not (derived-mode-p 'org-mode))
              ;; Force the mode for future visits.
              (let ((org-insert-mode-line-in-empty-file t)
                    (org-inhibit-startup t))
                (call-interactively 'org-mode)))
          ;; Force the TODO keywords etc. of the original buffer
          (let ((org-todo-line-regexp tr-org-todo-line-regexp)
                (org-todo-keywords-1 tr-org-todo-keywords-1)
                (org-todo-kwd-alist tr-org-todo-kwd-alist)
                (org-done-keywords tr-org-done-keywords)
                (org-todo-regexp tr-org-todo-regexp)
                (org-todo-line-regexp tr-org-todo-line-regexp)
                (org-odd-levels-only
                 (if (local-variable-p 'org-odd-levels-only (current-buffer))
                     org-odd-levels-only
                   tr-org-odd-levels-only)))
            (goto-char (point-min))
            (outline-show-all)
            ;; Find and/or create correct hierarchy
            (while org-tree
              (let ((child-list (org-archive-hierarchically--org-child-list)))
                (if (member (car org-tree) child-list)
                    (progn
                      (search-forward (car org-tree) nil t)
                      (setq org-tree (cdr org-tree)))
                  (progn
                    (newline)
                    (org-archive-hierarchically--insert-struct org-tree)
                    (setq org-tree nil)))))
            (newline) ; TODO neccessary at times? For first child
                                        ; under current org-tree?
            ;;Paste
            (let ((cl (or (org-current-level) 0)))
              (org-paste-subtree (org-get-valid-level cl 1)))
            ;; Shall we append inherited tags?
            (and inherited-tags
                 (or (and (eq org-archive-subtree-add-inherited-tags 'infile)
                          infile-p)
                     (eq org-archive-subtree-add-inherited-tags t))
                 (org-set-tags all-tags))
            ;; Mark the entry as done
            (when (and org-archive-mark-done
                       (looking-at org-todo-line-regexp)
                       (or (not (match-end 2))
                           (not (member (match-string 2) org-done-keywords))))
              (let (org-log-done org-todo-log-states)
                (org-todo
                 (car (or (member org-archive-mark-done org-done-keywords)
                          org-done-keywords)))))
            ;; Add the context info.
            (dolist (item org-archive-save-context-info)
              (let ((value (cdr (assq item context))))
                (when (org-string-nw-p value)
                  (org-entry-put
                   (point)
                   (concat "ARCHIVE_" (upcase (symbol-name item)))
                   value))))
            ;; Save the buffer, if it is not the same buffer.
            (unless (eq this-buffer buffer) (save-buffer)))))
      ;; Here we are back in the original buffer.  Everything seems
      ;; to have worked.  So now run hooks, cut the tree and finish
      ;; up.
      (run-hooks 'org-archive-hook)
      (let (this-command) (org-cut-subtree))
      (when (featurep 'org-inlinetask)
        (org-inlinetask-remove-END-maybe))
      (setq org-markers-to-move nil)
      (message "Subtree archived hierarchally in file: %s"
               (abbreviate-file-name afile))))
  (org-reveal)
  (if (looking-at "^[ \t]*$")
      (outline-next-visible-heading 1)))

(defun org-archive-hierarchically--line-content-as-string ()
  "Return the content of the current line as a string."
  (save-excursion
    (beginning-of-line)
    (buffer-substring-no-properties
     (line-beginning-position) (line-end-position))))

(defun org-archive-hierarchically--org-child-list ()
  "Return all children of a heading as a list."
  (save-excursion
    ;; this only works with org-version > 8.0, since in previous
    ;; org-mode versions the function (org-outline-level) returns
    ;; garbage when the point is not on a heading.
    (if (= (org-outline-level) 0)
        (outline-next-visible-heading 1)
      (org-goto-first-child))
    (let ((child-list (list (org-archive-hierarchically--line-content-as-string))))
      (while (org-goto-sibling)
        (setq child-list (cons (org-archive-hierarchically--line-content-as-string) child-list)))
      child-list)))

;; TODO, could be replaced by org-get-outline-path,
;; However that gives lists like:  ("level 1" "level 2"), while we
;; currently depend on lists like: ("* level 1" "** level 2")
;; so insert-struct would need to be passed level information as well
;; (making the recursion more complicated)
(defun org-archive-hierarchically--org-struct-subtree ()
  "Return the tree structure in which a subtree belongs as a list."
  (let ((archive-tree nil))
    (save-excursion
      (while (org-up-heading-safe)
        (let ((heading
               (buffer-substring-no-properties
                (line-beginning-position) (line-end-position))))
          (if (eq archive-tree nil)
              (setq archive-tree (list heading))
            (setq archive-tree (cons heading archive-tree))))))
    archive-tree))


(defun org-archive-hierarchically--insert-struct (struct)
  "Recursively insert a ‘org-mode’ structure given in STRUCT.
STRUCT is in the list format given by `org-archive-hierarchically-org-struct-subtree'"
  (interactive)
  (when struct
    (insert (car struct))
    (newline)
    (org-archive-hierarchically--insert-struct (cdr struct))))

(provide 'org-archive-hierarchically)

;;; org-archive-hierarchically.el ends here
